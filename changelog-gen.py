#!/usr/bin/env python3
"""
Usage:
    changelog-gen build [options] <source> <output>

Options:
    --link-format <format>
        Format string to generate links to diffs. Use {new} and {old} for the
        new and old version respectively.
    --link-format-initial <format>
        Format string to generate links to the first version's log.
    --squash-before <tag>
        Combines all changelogs before <tag> into one entry. Needed if the
        changelog was added mid-development. Can be HEAD to squash all changes
        into the "Unreleased" section.
"""

import re
from collections import namedtuple, OrderedDict
import functools
import itertools
from docopt import docopt
import sys

"""
1. Source -> [(key, type, message)]
2. Git history -> [(tag, [key])]
3. [(tag, {type, [message]})]
4. --> Markdown | reST
"""

def parse_source(source):
    """
        parse_source:: [line] -> {key, (type, message)}

    Transfors the contents of a source file into a list of (key, type, message)
    tuples.
    """

    keyline_regex = re.compile(r"\|(.+)\| (.+): ?(.*)", re.DOTALL)
    FoldState = namedtuple('FoldState', ['curr', 'hist'])
    Entry = namedtuple('Entry', ['key', 'type', 'message'])

    def hist_with_entry(hist, entry):
        if entry == None:
            return hist
        else:
            return {entry.key: namedtuple("Entry", ["type", "message"])(entry.type, entry.message), **hist}

    def entry_with_message_appended(entry, text):
        return Entry(entry.key, entry.type, entry.message + text)

    def entry_from_match(m):
        return Entry(m.group(1), m.group(2), m.group(3))

    def fold_func(state, line):
        if line.startswith("#") or line.isspace() or line == "":
            return state
        elif line.startswith("|"):
            sections = keyline_regex.fullmatch(line)
            return FoldState(entry_from_match(sections), hist_with_entry(state.hist, state.curr))
        else:
            return FoldState(entry_with_message_appended(state.curr, line), state.hist)
    final_state = functools.reduce(fold_func, source, FoldState(None, {}))
    return hist_with_entry(final_state.hist, final_state.curr)

def generate_tag_keys(source_name, last_tag):
    """
        generate_tag_keys:: () -> IO [(tag, [key], date)]

    Parses the git history to create a list of tags (in commit order)
    and the keys their changelogs contained.
    """

    import subprocess

    def get_git_output(commands):
        return subprocess.run(["git"] + commands, stdout=subprocess.PIPE, stderr=subprocess.DEVNULL)

    def get_all_tags_before(commitish):
        yield commitish
        result = get_git_output(["describe", "--abbrev=0", commitish + "^"])
        tag = result.stdout.decode('utf-8').rstrip()
        if result.returncode == 0 and commitish != last_tag:
            yield from get_all_tags_before(tag)
        else:
            return

    def get_all_tags():
        return get_all_tags_before("HEAD")

    def tag_date(tag):
        return (get_git_output(["tag", "-l", "--format=%(taggerdate:iso8601)", tag])
            .stdout.decode("utf-8").strip())

    def tag_keys(tag):
        file_at_tag = get_git_output(["show", tag + ":" + source_name]).stdout.decode('utf-8')
        return parse_source(file_at_tag.splitlines()).keys()

    tags = list(get_all_tags())

    def added_keys_in_tags():
        keys_in_tags = [set(tag_keys(tag)) for tag in tags]
        for (key_set, prev_key_set) in itertools.zip_longest(keys_in_tags, keys_in_tags[1:]):
            yield key_set - (prev_key_set or set())


    Tag = namedtuple("Tag", ["name", "keys", "date"])
    return (Tag(tag, added_keys, tag_date(tag)) for tag, added_keys in zip(tags, added_keys_in_tags()))


def changelog_model(key_descriptions, tags):
    """
        changelog_model:: {key: Entry(type, message)} -> [Tag(name, keys, date)]
            -> OrderedDict(name, TagData(date, {type: message}))
    """
    TagData = namedtuple("TagData", ["date", "message_by_type"])

    def messages_by_type(tag):
        # [(k, (type, message)]
        entries_for_tag = sorted(entry for (key, entry) in key_descriptions.items() if key in tag.keys)
        return {k: [e.message for e in g] for k, g in itertools.groupby(entries_for_tag, key=lambda x: x.type)}

    return OrderedDict((tag.name, TagData(tag.date, messages_by_type(tag))) for tag in tags)


def model_to_markdown(model, first_version_link, version_link):
    """
        model_to_markdown:: model -> string
    """

    preamble = """# Changelog
All notable changes to this project are documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0).

"""

    def heading_from_tag(tag):
        if tag == "HEAD":
            return "Unreleased"
        elif tag.startswith("v"):
            return tag[1:]
        else:
            return tag

    def link_from_tag(tag, prevVersion):
        if prevVersion and version_link:
            return version_link.format(new=tag, old=prevVersion[0])
        elif (not prevVersion) and first_version_link:
            return first_version_link.format(tag)
        else:
            return None

    def skip_first(o):
        iterobj = iter(o)
        next(iterobj)
        return iterobj

    result = ""
    result += preamble
    for ((tag, (date, categories)), prevVersion) in itertools.zip_longest(model.items(), skip_first(model.items())):
        date_suffix = " - {}".format(date.split(" ")[0]) if date else ""
        link = link_from_tag(tag, prevVersion)
        heading = heading_from_tag(tag)
        full_heading = "[{}]({})".format(heading, link) if link else heading
        result += "\n## {}{}\n".format(full_heading, date_suffix)
        for (category, entries) in categories.items():
            result += "\n### {}\n".format(category)
            for entry in entries:
                result += "- {}\n".format(entry)
    return result

def main():
    arguments = docopt(__doc__)
    if arguments["build"]:
        source_name = arguments["<source>"]
        with open(source_name) as source:
            key_descriptions = parse_source(source.read().splitlines())
        tag_keys = generate_tag_keys(source_name, arguments["--squash-before"])
        model = changelog_model(key_descriptions, tag_keys)
        output_name = arguments["<output>"]
        with open(output_name, "w") if output_name != "-" else sys.stdout as output:
            output.write(model_to_markdown(model, arguments["--link-format-initial"], arguments["--link-format"],))

if __name__ == "__main__":
    main()
