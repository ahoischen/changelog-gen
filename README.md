# Changelog Generator

> Don't "Version Bump" your changelog!

This project stops you from having to update your changelog on each release, by
generating it from a human-readable format. It does *not* dump your git commits,
see https://keepachangelog.com on why that is terrible.


## Getting started

You'll need Python3 and docopt installed. Create a file for your changelog's
source and add the following line:

```
|add-changelog-gen| Added: Automatically generated changelog
```

Now run `python3 changelog-gen.py --squash-before HEAD ./my-changelog-source ./CHANGELOG.md`
and you should see something like this:

```markdown
## Unreleased

### Added
- Automatically generated changelog
```

Now commit and create an annotated tag. Your changes will now appear in a
section for that tag with the tag's date.

## Usage

This tool works by having a changelog source which is tracked by version control
and updated whenver a feature is added. The changelog source consists of entries
with keys and types. Each entry must start with an arbitrary single-line key
between `|`, followed by a space, the change's type (e.g. "Added", "Deprecated", ...),
a `:` and a description. It is possible to use multiple lines for the
description, as long as they don't start with a `|` or `#` (whitespace is
significant here). Comments are lines starting with a `#`. See [./changelog-source.txt](./changelog-source.txt)
for an example.

The keys should be unique throughout the changelog and should never be changed.
They are used to track entries, even if their text is changed (spelling fixes etc.).

It is possible to generate links to diffs using the `--link-format[-initial]` options.


## License

GPLv3 (you can of course generate changelogs without adopting that license)
